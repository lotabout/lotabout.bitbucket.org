#!/bin/sh

COMMIT_MSG=${1:-"Auto Generated $(date +"%D")"}
DEPLOY_BRANCH=${DEPLOY_BRANCH:-"master"}
SOURCE_BRANCH=${SOURCE_BRANCH:-"source"}
cd _site
cp -r ../.git .
git add .
git add -u
git commit -m "$COMMIT_MSG"
echo "Pushing generated site"
git push -u origin $SOURCE_BRANCH:$DEPLOY_BRANCH --force
echo "Deploy pushing completed."
